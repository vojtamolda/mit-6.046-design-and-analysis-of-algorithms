# MIT Course 6.046 - Design and Analysis of Algorithms (Spring 2015)

Lecture videos, exams, assignments and other materials are downloaded from [MIT OpenCourseWare] (http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-046j-design-and-analysis-of-algorithms-spring-2015/index.htm).

## Prerequisites

This course is the header course for the Theory of Computation concentration. You are expected, and strongly
encouraged, to have taken:

 - 6.006 - [Introduction to Algorithms](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/index.htm)
 - 6.042J - [Mathematics for Computer Science](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-fall-2010/)


## Course Description

This course assumes that students know how to analyze simple algorithms and data structures from having taken 6.006.
It introduces students to the design of computer algorithms, as well as analysis of sophisticated algorithms.

## Assignments

We will assign seven problem sets during the course of the semester. Each problem set will consist of
a programming assignment, to be completed in Python, and a theory assignment.

## Textbook

Cormen, Thomas, Charles Leiserson: [Introduction to Algorithms](introduction to algorithms.pdf), MIT Press, 2009. ISBN: 9780262033848.
